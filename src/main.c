#include "mem.h" 
#include <stdbool.h>
#include <stdio.h>

#define SIZE 0x1024
#define BLOCK_SIZE 0x100
#define BLOCK_SIZE2 0x200

bool run_memory_allocator_tests();
bool perform_normal_allocation_test();
bool perform_free_single_block_test();
bool perform_free_multiple_blocks_test();
bool perform_expand_existing_region_test();
bool perform_allocate_new_region_test();

int main() {
    printf("Running memory allocator tests...\n");

    int tests_passed = run_memory_allocator_tests();

    printf("Total tests passed: %d/%d\n", tests_passed, 5);
    return 0;
}

bool run_memory_allocator_tests() {
    int tests_passed = 0;
    tests_passed += perform_normal_allocation_test();
    tests_passed += perform_free_single_block_test();
    tests_passed += perform_free_multiple_blocks_test();
    tests_passed += perform_expand_existing_region_test();
    tests_passed += perform_allocate_new_region_test();
    return tests_passed == 5;
}

bool perform_normal_allocation_test() {
    void* memory_block = _malloc(BLOCK_SIZE);
    if (memory_block == NULL) {
        printf("Test normal allocation: Failed\n");
        return false;
    }
    _free(memory_block);
    printf("Test normal allocation: Passed\n");
    return true;
}

bool perform_free_single_block_test() {
    void* block1 = _malloc(BLOCK_SIZE);
    void* block2 = _malloc(BLOCK_SIZE2
    );
    _free(block1);
    if (block2 == NULL) {
        printf("Test free single block: Failed\n");
        return false;
    }
    _free(block2);
    printf("Test free single block: Passed\n");
    return true;
}

bool perform_free_multiple_blocks_test() {
    void* block1 = _malloc(BLOCK_SIZE);
    void* block2 = _malloc(BLOCK_SIZE2);
    _free(block1);
    _free(block2);
    printf("Test free multiple blocks: Passed\n");
    return true;
}

bool perform_expand_existing_region_test() {
    void* block1 = _malloc(BLOCK_SIZE);
    void* block2 = _malloc(SIZE * SIZE);
    if (block2 == NULL) {
        _free(block1);
        printf("Test expand existing region: Failed\n");
        return false;
    }
    _free(block1);
    _free(block2);
    printf("Test expand existing region: Passed\n");
    return true;
}

bool perform_allocate_new_region_test() {
    void* block1 = _malloc(SIZE * SIZE);
    void* block2 = _malloc(SIZE * SIZE);
    if (block2 == NULL) {
        _free(block1);
        printf("Test allocate new region: Failed\n");
        return false;
    }
    _free(block1);
    _free(block2);
    printf("Test allocate new region: Passed\n");
    return true;
}
